source('/home/admin/CODE/common/aggregate.R')

registerMeterList("SG-005S",c("Fab2","Fab35","Fab71","Fab72","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 46 #Column no for DA
aggColTemplate[3] = 44 #column for LastRead
aggColTemplate[4] = 45 #column for LastTime
aggColTemplate[5] = 36 #column for Eac-1
aggColTemplate[6] = 37 #column for Eac-2
aggColTemplate[7] = 38 #column for Yld-1
aggColTemplate[8] = 39 #column for Yld-2
aggColTemplate[9] = 40 #column for PR-1
aggColTemplate[10] = 41 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("SG-005S","Fab2",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 46 #Column no for DA
aggColTemplate[3] = 54 #column for LastRead
aggColTemplate[4] = 53 #column for LastTime
aggColTemplate[5] = 47 #column for Eac-1
aggColTemplate[6] = 48 #column for Eac-2
aggColTemplate[7] = 49 #column for Yld-1
aggColTemplate[8] = 50 #column for Yld-2
aggColTemplate[9] = 41 #column for PR-1
aggColTemplate[10] = 52 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("SG-005S","Fab35",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 46 #Column no for DA
aggColTemplate[3] = 62 #column for LastRead
aggColTemplate[4] = 61 #column for LastTime
aggColTemplate[5] = 55 #column for Eac-1
aggColTemplate[6] = 56 #column for Eac-2
aggColTemplate[7] = 57 #column for Yld-1
aggColTemplate[8] = 58 #column for Yld-2
aggColTemplate[9] = 59 #column for PR-1
aggColTemplate[10] = 60 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("SG-005S","Fab71",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 46 #Column no for DA
aggColTemplate[3] = 70 #column for LastRead
aggColTemplate[4] = 69 #column for LastTime
aggColTemplate[5] = 63 #column for Eac-1
aggColTemplate[6] = 64 #column for Eac-2
aggColTemplate[7] = 65 #column for Yld-1
aggColTemplate[8] = 66 #column for Yld-2
aggColTemplate[9] = 67 #column for PR-1
aggColTemplate[10] = 68 #column for PR-2
aggColTemplate[11] = 4 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("SG-005S","Fab72",aggNameTemplate,aggColTemplate)

