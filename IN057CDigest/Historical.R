rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN057CHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN057CDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(5,6,1,2,3,4)

if(RESETHISTORICAL)
{
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-057C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-057C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-057C]')
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-057C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-057C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-057C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-057C]"

checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 1 : length(years))
{
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 1 : length(months))
  {
    pathmon = paste(pathyr,months[y],sep="/")
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    
    for(t in 1 : length(stns))
    {
      type = 1
      pathstn = paste(pathmon,stns[t],sep="/")
      if(grepl("MFM",stns[t]))
        type = 0
      if(stns[t] == "WMS")
        type = 2
      pathwritestn = paste(pathwritemon,stns[t],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        if(t == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
sink()
