rm(list=ls(all =TRUE))

pathRead <- "~/intern/data/[717]/2018/"

pathwritetxt <- 'C:/Users/talki/Desktop/cec intern/results/717/[717]_summary.txt'
pathwritecsv <- 'C:/Users/talki/Desktop/cec intern/results/717/[717]_summary.csv'

setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2017-01-07 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-09-12 17:00:00"), format="%H:%M:%S")
nameofStation <- "717"
#stationLimit <- 1628.2
col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = 0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(paste(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #gsi
  gsi <- as.numeric(temp[,3])
  col4[index] <- sum(gsi)/60000
  
  #hamb&tamb
  #tamb <- mean(as.numeric(temp[,4]))
  #col5[index] <- tamb
  #hamb <- mean(as.numeric(temp[,5]))
  #col6[index] <- hamb
  
  
  ex_proc <- index/length(filelist)*100
  print(paste(round(ex_proc,1), "%"))
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
#col5 <- col5[1:(index-1)]
#col6 <- col6[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
#col5[is.na(col5)] <- NA
#col6[is.na(col6)] <- NA

result <- cbind(col0,col1,col2,col3,col4)
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points","GSI")
result[,3] <- round(as.numeric(result[,3]),2)
result[,5] <- round(as.numeric(result[,5]),2)
#result[,6] <- round(as.numeric(result[,6]),2)
#result[,7] <- round(as.numeric(result[,7]),1)

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
dataWrite <- result
write.table(result,na = "",pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,pathwritecsv, na= "", row.names = FALSE)


library(ggplot2)

g <- ggplot(data=result, aes(x= as.Date(result[,2]), y=result[,5])) + geom_point(size=0.9)
g <- g + theme_bw() + xlab("Date") + ylab("GSI")
g <- g + ggtitle(paste0(nameofStation," Daily irradiance"))
g <- g + theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
g <- g + theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
g <- g + theme(axis.text.x = element_text(size=11))
g <- g + theme(axis.text.y = element_text(size=11))
g <- g + theme(plot.title = element_text(face = "bold",size= 11,lineheight = 0.7,hjust = 0.5))
g <- g + theme(plot.title=element_text(margin=margin(0,0,7,0)))

ggsave(g,filename = paste0('C:/Users/talki/Desktop/cec intern/results/717/',nameofStation, "daily irradiance.pdf"),width =7.92, height =5)

