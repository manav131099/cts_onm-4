rm(list=ls(all =TRUE))
library(ggplot2)

readtxt<- read.table("/home/admin/ws_coim.txt", header= TRUE, sep= "\t")
readtxt[,1] = as.Date(readtxt[,1])
themesettings1 <- theme(plot.title = element_text(hjust = 0.5),                #title alignment
                        axis.title.x = element_text(size=19),                  # x axis label size
                        axis.title.y = element_text(size=19),                  # y axis label size
                        axis.text = element_text(size=11),                     # axes text size
                        panel.grid.minor = element_blank(),                    #remove minor grid lines
                        panel.border = element_blank(),                        #remove plot border
                        axis.line = element_line(colour = "black"),            # axis line colour
                        legend.justification = c(1, 1),                        #legend in plot
                        legend.position = c(1, 1))                             #legend position x,y
g1 <- ggplot(readtxt) + theme_bw() + themesettings1
g1 <- g1 + geom_line(aes(x=Date, y=WSMean, colour = "Mean"))
g1 <- g1 + geom_line(aes(x=Date, y=WSMax, colour = "Max"))
g1 <- g1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")   
g1 <- g1 + xlab('Month') + ylab('Wind speed')
g1 <- g1 + scale_colour_manual('', values = c("Mean"="blue", "Max"="orange2"))  
g1

ggsave(g1, filename = "/home/admin/Jason/cec intern/results/wsgraph_Coim_may_18.pdf",width = 10.92, height = 5)
