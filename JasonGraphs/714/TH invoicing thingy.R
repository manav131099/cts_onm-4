################
################
rm(list=ls(all =TRUE))

library(ggplot2)
library(scales)

rf = function(x){
  return(format(round(x,2),nsmall=2))
}

lm_eqn <- function(x,y,data)
{
  m <- lm(y ~ x, data);
  eq <- substitute(italic(y) == a + b %.% italic(x)*","~~italic(r)^2~"="~r2, 
                   list(a = format(coef(m)[1], digits = 2), 
                        b = format(coef(m)[2], digits = 2), 
                        r2 = format(summary(m)$r.squared, digits = 3)))
  as.character(as.expression(eq));                 
}

themesettings1 <- theme(plot.title = element_text(hjust = 0.5), axis.title.x = element_text(size=19), axis.title.y = element_text(size=19), 
                        axis.text = element_text(size=11), 
                        panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                        legend.justification = c(1, 1), legend.position = c(1, 1))

titlemonth = "714"
#allocating column number
date=1
gsi = 3
ac = 71
#

path = "~/intern/Data/[714]/2017/2017-03"

setwd(path)
filelist <- dir(pattern = ".txt", recursive= TRUE)
dffg=dff=NULL
for(z in filelist[1:length(filelist)]){
  temp <- read.table(z, header =T, sep= '\t', stringsAsFactors = F)
  
  pacTemp <- temp
  irrad <- as.numeric(paste(pacTemp[,gsi]))
  pacTemp1 <- pacTemp[complete.cases(as.numeric(paste(abs(pacTemp[,ac])))),]
  pacpoints <- abs(as.numeric(as.character(pacTemp[,ac])))
  gsipoints <- abs(irrad)
  timestamps <- cbind(pacTemp[,date],gsipoints)
  dffg <- rbind(dffg,data.frame(timestamps))
  dff <- rbind(dff,data.frame(pacpoints))
  print(paste(z, "done"))
}

dff=data.frame(dff)
dffg=data.frame(dffg)
dffg=cbind(dffg,dff)

#conversion
dffg$pacpoints <- (dffg$pacpoints/63)*1404

#melt
dffg$date <- as.Date(substr(dffg$V1,0,10))

df <- with(dffg, dffg[(date > "2017-03-04" & date < "2017-03-12"), ])
df$V1 <- as.POSIXct(df$V1, format="%Y-%m-%d %H:%M")

#sort
timemin <- format(as.POSIXct("2017-01-10 08:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-11-06 22:01:00"), format="%H:%M:%S")

df$sort = 0

df$sort[format(as.POSIXct(df$V1), format = "%H:%M:%S") > timemin & format(as.POSIXct(df$V1), format="%H:%M:%S") <timemax] <- "peak"  #sort peak and non peak timings

df$sort[format(df$date, format="%w") == 0 | format(df$date, format="%w") == 6] <- "nonpeak"      #sort sunday and saturday
df$sort[df$sort == 0] <- "nonpeak"

cols <- c("peak" = "red", "nonpeak" = "lightblue")

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_line(data=df, aes(x=V1,y=pacpoints, color = sort, group =1)) + ylab("Power AC [kW]") + xlab("Time")
p1 <- p1 +  themesettings1
p1 <- p1 + scale_colour_manual('Period' ,labels = c('Off-peak','Peak'), 
                               values = cols)
#p1 <- p1 + scale_x_datetime(labels = date_format("%Y-%m-%d"),date_breaks = "day")
#p1 <- p1 + theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust =0.5, size = 11)) 

p1

ggsave(paste0('~/intern/Results/[714]/1. result/Pac 1 week plot (line).pdf'), p1, width =11, height=6)

#binning for period

#dfbin <- df$pacpoints[df$pacpoints > 5/1000]

peakbin <- ((length(df[,1][df$pacpoints > (5/1000) & df$sort == "peak"]))/length(df[,1]))*100
nonpeakbin <- ((length(df[,1][df$pacpoints > (5/1000) & df$sort == "nonpeak"]))/length(df[,1]))*100
bindata <- data.frame(t(data.frame(peakpoints = peakbin, nonpeakpoints = nonpeakbin)))

peakenergy <- rf(sum(df$pacpoints[df$sort == "peak"])/60000)              #sum of period  7 days
nonpeakenergy <- rf(sum(df$pacpoints[df$sort == "nonpeak"])/60000)       

colnames(bindata) <- "datapoints"

p2 <- ggplot(data=bindata, aes(x=1:2, y=datapoints)) + theme_bw()
p2 <- p2 + geom_bar(stat="identity", colour="orange", fill="red", width = 0.35)
p2 <- p2 + ylab('Frequency of occurences of Pac points > 5 W [%]') + xlab('')
p2 <- p2 + scale_x_continuous(breaks = c(1,2), labels = c("Peak period", "Off-peak period"))
p2 <- p2 + coord_cartesian(xlim = c(0.5,2.5), ylim = c(0,70))
p2 <- p2 + theme(plot.title = element_text(hjust = 0.5), axis.title = element_text(size=15), 
                 axis.text = element_text(size=11), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"))
p2

ggsave(paste0('~/intern/Results/[714]/1. result/bar chart.pdf'), p2, width =11, height=6)

#time series for single day
eac_peak[7] = eac_offpeak[7] = 0

df2 <- with(df, df[(date > "2017-03-10" & date < "2017-03-12"), ])

eac_peak[7] = sum(df2$pacpoints[df2$sort == "peak"])/60000
eac_offpeak[7] <- sum(df2$pacpoints[df2$sort == "nonpeak"])/60000

df3 <- df2[as.numeric(substr(df2[,1],12,13)) >= 6 & as.numeric(substr(df2[,1],12,13)) < 21,]

p3 <- ggplot() + theme_bw()
p3 <- p3 + geom_line(data = df3, aes(x = 1:900, y = pacpoints, color = sort), size = 1.3) + ylab("Power AC [kW]") + xlab("Time")
p3 <- p3 +  themesettings1
p3 <- p3 + scale_colour_manual('Period' ,labels = c('Off-peak','Peak'), 
                               values = cols)
p3 <- p3 + scale_x_continuous(breaks = c(1,180,360,540,720,900), labels = c("06:00","09:00","12:00","15:00","18:00","21:00"))

p3

#energy generated within bins

ggsave(paste0('~/intern/Results/[714]/1. result/1 day pac plot (2017-03-11).pdf'), p3, width =11, height=6)

df2 <- with(df, df[(date > "2017-03-04" & date < "2017-03-06"), ])

p4 <- ggplot() + theme_bw()
p4 <- p4 + geom_line(data = df2, aes(x = 1:1440, y = pacpoints, color = sort, group = 1), size = 1.3) + ylab("Power AC [kW]") + xlab("Time")
p4 <- p4 +  themesettings1
p4 <- p4 + scale_colour_manual('Period' ,labels = c('Off-peak','Peak'), 
                               values = cols)
p4 <- p4 + scale_x_continuous(breaks = c(1,361,721,1081,1440), labels = c("00:00","06:00","12:00","18:00","23:59"))

p4

ggsave(paste0('~/intern/Results/[714]/1. result/1 day pac plot (2017-03-05).pdf'), p4, width =11, height=6)


write.table(df,na = "",'~/intern/Results/[714]/1. result/melted data.txt',row.names = FALSE,sep ="\t")
