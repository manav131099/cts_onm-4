#Uptime graph generation
#Use this to plot uptime + supporting graphs
#Condition: only computes station with 1 meter
##################################################################################################
rm(list=ls(all =TRUE))
#package files required

library(ggplot2)

#enter file to read in

result <- read.csv("~/intern/Results/[713]/1. result/[713]_da_summary.csv")

#output directory and filename

pathWrite <- "~/intern/Results/[713]/1. result/"

#station name

station <- "[713]"
##################################################################################################


rownames(result) <- NULL
result <- data.frame(result)
result <- result[,(-1)] #removing first column
colnames(result) <- c("date","da","pts","NApts","pts2","da2","spm10")

result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))

first <- result[,1]
last <- tail(first,1)

result[,2][result[,2]>100] <- NA


dagraph <- ggplot(result, aes(x=date)) + ylab("Availability [%]") + xlab("Time")
da1 <- dagraph + geom_line(aes(y=da), size=1.1, colour = "red") + geom_line(aes(y=da2), size =0.55, colour = "blue2")
da1 <- da1 + theme_bw()
da1 <- da1 + expand_limits(x=first[1],y=0)
da1 <- da1 + scale_y_continuous(breaks=seq(0, 100, 10))
da1 <- da1 + scale_x_date(date_breaks = "1 month",date_labels = "%b")
da1 <- da1 + ggtitle(paste0(station," Data Availability"), subtitle=paste0("From ", first[1]," to ",last))
da1 <- da1 + theme(axis.title.x = element_text(size=19), axis.title.y = element_text(size=19),
                   axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(), axis.line = element_line(colour = "black"))
da1 <- da1 + theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin=margin(0,0,7,0)),
                  plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))
da1 <- da1 + annotate("text",label = paste0("Average DA = ", round(mean(result[,2], na.rm=TRUE),2),"%"), size=3.7,
                      x = as.Date(first[round(0.518*length(first))]), y= 86, colour = "red")
da1 <- da1 + annotate("text",label = paste0("Average DA (6am-8pm) = ", round(mean(result[,6], na.rm=TRUE),2),"%"), size=3.7,
                      x = as.Date(first[round(0.518*length(first))]), y= 77, colour = "blue")
da1 <- da1 + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left
da1

ggsave(da1,filename = paste0(pathWrite,station,"_DA.pdf"), width =7.92, height =5)

